'use strict';

function clockWebsite() {
  const clockTime = new Date();

  let hours = clockTime.getHours();
  let minutes = clockTime.getMinutes();
  let seconds = clockTime.getSeconds();

  hours = hours < 10 ? '0' + hours : hours;
  minutes = minutes < 10 ? '0' + minutes : minutes;
  seconds = seconds < 10 ? '0' + seconds : seconds;

  const webClock = hours + ':' + minutes + ':' + seconds;

  const divElement = document.createElement('div');
  const body = document.querySelector('body');
  body.appendChild(divElement);

  document.querySelector('div').textContent = webClock;

  setInterval(clockWebsite, 1000);
}

clockWebsite();
