'use strict';

function randomNumber() {
  let aleatorio = Math.floor(Math.random() * 101);

  let userNumber;

  for (let intentos = 0; intentos < 5; intentos++) {
    userNumber = parseInt(prompt('Introduzca un número entre el 0 y el 100'));

    if (userNumber === aleatorio) {
      break;
    } else if (userNumber > aleatorio) {
      alert('La contraseña es un número menor al introducido.');
    } else if (userNumber < aleatorio)
      alert('La contraseña es un número mayor al introducido.');
  }
  if (userNumber === aleatorio) {
    alert('¡Enhorabuena!, ¡el número es el ' + aleatorio + '!');
  } else {
    alert('¡Has perdido!');
  }
}

randomNumber();
